const data = require('../data/data');
const fs = require('fs');
const path = require('path');


const matchesData = data().matches();
const deliveriesData = data().deliveries();

function saveJsonToFile(jsonData, fileName) {
  const outputFilePath = path.join(__dirname, '..', 'public', 'output', fileName);

  fs.writeFileSync(outputFilePath, JSON.stringify(jsonData, null, 2), 'utf8');

  console.log(`JSON data saved to ${outputFilePath}`);
}

 
saveJsonToFile(matchesData, 'matches.json');
saveJsonToFile(deliveriesData, 'deliveries.json');

module.exports = saveJsonToFile;