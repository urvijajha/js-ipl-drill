const data = require('../data/data');
const saveJsonToFile = require('../server/index');


const matches = data().matches();

function matchesWonPerTeamPerYear(matches){
    try {
        const matchesPerTeamPerYear = {};

    for(let match of matches){

        if(matchesPerTeamPerYear[match.season]=== undefined){
            matchesPerTeamPerYear[match.season]={};
        }

        if(matchesPerTeamPerYear[match.season][match.winner]){
            matchesPerTeamPerYear[match.season][match.winner]++;
        }
        else{
            matchesPerTeamPerYear[match.season][match.winner] = 1;
        }
    }

    return matchesPerTeamPerYear;
    } catch (error) {
        console.log(`Error in number of wins per year for each team. ${error}`);
    }
}

const result = matchesWonPerTeamPerYear(matches);

saveJsonToFile(result, "matchesWonPerTeamPerYear.json");

// console.log(result);