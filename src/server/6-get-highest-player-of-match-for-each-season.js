const data = require("../data/data");
const saveDataToJSON = require("../server/index");

const matches = data().matches();

function highestPlayerOfMatch(matches){
  try {
    const playerOfMatchCounts = {};

    for(let match of matches){
        if(playerOfMatchCounts[match.season] === undefined){
            playerOfMatchCounts[match.season]={};
        }

        if(playerOfMatchCounts[match.season][match.player_of_match]){
            playerOfMatchCounts[match.season][match.player_of_match]++;
        }
        else{
            playerOfMatchCounts[match.season][match.player_of_match]=1;
        }
    }
    

    
    for(let season in playerOfMatchCounts){
        let highestCountOfPlayer = "";
        let maxi = 0;

        for(let player in playerOfMatchCounts[season]){
            if(maxi < playerOfMatchCounts[season][player]){
                maxi = playerOfMatchCounts[season][player];
                highestCountOfPlayer = player;
            }
        }

        playerOfMatchCounts[season] = highestCountOfPlayer;
    }
    return playerOfMatchCounts;
  } catch (error) {
    console.log(`Error in player with highest player of match for each season. ${error}`);
  }

    // console.log(playerOfMatchCounts);
}

const result = highestPlayerOfMatch(matches);
// console.log(result);
saveDataToJSON(result, "highestPlayerOfMatchForEachSeason.json");