const data = require('../data/data');
const saveJsonToFile = require('../server/index');

let matches = data().matches();

function matchesPerYear(matches) {
  try {
    const matchesYear = {};

  for (let match of matches) {
    if (matchesYear[match.season]) {
      matchesYear[match.season]++;
    } else {
      matchesYear[match.season] = 1;
    }
  }

  return matchesYear;
  } catch (error) {
    console.log(`Error in number of matches per year. ${error}`);
  }
}

const result = matchesPerYear(matches);
saveJsonToFile(result , "matchesPerYear.json");