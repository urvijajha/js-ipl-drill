const data = require("../data/data");
const saveJsonToFile = require("../server/index");

const matches = data().matches();
const deliveries = data().deliveries();


function topEconomicalBowlers2015(matches, deliveries){

    const economicalBowler = {};
    const matchInYear = [];

    for(let match of matches){
        if(match.season === '2015'){
            matchInYear.push(match);
        }
    }

    for(let matchYear of matchInYear){
        const deliveriesInMatch = [];
        
        for(let delivery of deliveries){
            if(matchYear.id == delivery.match_id){
                deliveriesInMatch.push(delivery);
            }
        }

        for (let delivery of deliveriesInMatch) {
            let isLegalBall = delivery.noball_runs === '0' && delivery.wide_runs === '0';

            
            if (economicalBowler[delivery.bowler] === undefined) {
                economicalBowler[delivery.bowler] = { balls: 0, runs: 0 };
            }

            if (isLegalBall) {
                economicalBowler[delivery.bowler].balls++;
            }

            economicalBowler[delivery.bowler].runs += parseInt(delivery.total_runs);
        }
    }

    
    let economyRates = [];
    for (let bowler in economicalBowler) {
        let overs = economicalBowler[bowler].balls / 6;
        let economyRate = economicalBowler[bowler].runs / overs;

        economyRates.push({ bowler: bowler, economyRate: economyRate });
    }

    
    economyRates.sort((a, b) => a.economyRate - b.economyRate);

   
    let result = {};
    for (let index = 0; index < 10 && index < economyRates.length; index++) {
        result[economyRates[index].bowler] = economyRates[index].economyRate
    }

    return result;

}

const result = topEconomicalBowlers2015(matches, deliveries);

saveJsonToFile(result, "top10EconomicalPlayers2015.json");

// console.log(result);

