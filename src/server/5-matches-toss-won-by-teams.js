const saveJsonToFile = require(".");
const data = require("../data/data");

const matches = data().matches();

function countTimesTeamsWonTheTossAndTheMatch(matches){
    const wonTeam ={};

    for(let match of matches){
        if(match.toss_winner=== match.winner){
            if(wonTeam[match.winner]){
                wonTeam[match.winner]++;
            }
            else{
                wonTeam[match.winner]=1;
            }
        }
    }

    return wonTeam;
}

const result = countTimesTeamsWonTheTossAndTheMatch(matches);

saveJsonToFile(result, "timesTeamsWonTheTossAndTheMatch.json");

console.log(result);
