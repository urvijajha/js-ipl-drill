const data = require("../data/data");
const saveDataToJSON = require("../server/index");

const deliveries = data().deliveries();

function highestPlayerDismission(deliveries) {
   try {
    const playerDismission = {};

    for (let delivery of deliveries) {
        if (!playerDismission[delivery.bowler]) {
            playerDismission[delivery.bowler] = {};
        }

        if (delivery.player_dismissed && delivery.dismissal_kind!=='run out') {
            if (playerDismission[delivery.bowler][delivery.player_dismissed]) {
                playerDismission[delivery.bowler][delivery.player_dismissed]++;
            } else {
                playerDismission[delivery.bowler][delivery.player_dismissed] = 1;
            }
        }
    }

    let highestPlayerDismissed = "";
    let highestBowler = "";
    let maxi = 0;

    for (let bowler in playerDismission) {
        for (let batsman in playerDismission[bowler]) {
            if (playerDismission[bowler][batsman] > maxi) {
                maxi = playerDismission[bowler][batsman];
                highestPlayerDismissed = batsman;
                highestBowler = bowler;
            }
        }
    }

    let result = {
        bowler: highestBowler,
        batsman: highestPlayerDismissed,
        dismissals: maxi
    };

    return result;
   } catch (error) {
    console.log(`Error getting highest player dismissal pair and count. ${error}`);
   }
}

let result = highestPlayerDismission(deliveries);
// console.log(result);
saveDataToJSON(result, "highestPlayerDismissed.json");