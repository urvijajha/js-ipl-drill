const saveJsonToFile = require('.');
const data = require('../data/data');

const matches = data().matches();
const deliveries = data().deliveries();

function extraRunsIn2016(matches, deliveries){
    try {
        
        const extraRunsPerTeam = {};
        const matchInYear = [];
    
        for(let match of matches){
            if(match.season == 2016){
                matchInYear.push(match);
            }
        }
    
        for(let matchYear of matchInYear){
            const deliveriesInMatch = [];
            for(let delivery of deliveries){
                if(matchYear.id === delivery.match_id){
                    deliveriesInMatch.push(delivery);
                }
            }
    
            for(let runs of deliveriesInMatch){
                if(extraRunsPerTeam[runs.bowling_team]){
                    extraRunsPerTeam[runs.bowling_team] += parseInt(runs.extra_runs);
                }
                else{
                    extraRunsPerTeam[runs.bowling_team]=parseInt(runs.extra_runs);
                }
            }
            
        }
    
        return extraRunsPerTeam;
    } catch (error) {
        console.log(`Error in number of extra runs per team in year 2016. ${error}`);
    }
}

const result = extraRunsIn2016(matches, deliveries);

saveJsonToFile(result, "extraRunsPerTeam2016.json");

// console.log(result);