const data = require("../data/data");
const saveDataToJSON = require("../server/index");


const matches = data().matches();
const deliveries = data().deliveries();

function strikeRateOfBatsman(matches, deliveries) {
  const matchId = [];
  const strikeRate = {};

  
  for (let match of matches) {
    matchId.push(match);
  }

  
  for (let matchYear of matchId) {
    const deliveriesInMatch = [];
    for (let delivery of deliveries) {
      if (delivery.match_id == matchYear.id) {
        deliveriesInMatch.push(delivery);
      }
    }

   
    for (let delivery of deliveriesInMatch) {
      let isLegalBall = delivery.noball_runs === '0' && delivery.wide_runs === '0';

      if (!strikeRate[matchYear.season]) {
        strikeRate[matchYear.season] = {};
      }

      if (!strikeRate[matchYear.season][delivery.batsman]) {
        strikeRate[matchYear.season][delivery.batsman] = { runs: 0, balls: 0 };
      }

      if (isLegalBall) {
        strikeRate[matchYear.season][delivery.batsman].balls++;
      }

      strikeRate[matchYear.season][delivery.batsman].runs += parseInt(delivery.batsman_runs);
    }
  }

 
  const strikeRateResult = {};
  for (let season in strikeRate) {
    strikeRateResult[season] = {};
    for (let batsman in strikeRate[season]) {
      const stats = strikeRate[season][batsman];
      const strikeRateValue = (stats.runs / stats.balls) * 100;
      strikeRateResult[season][batsman] = strikeRateValue.toFixed(2);
    }
  }

  return strikeRateResult;
}

const result = strikeRateOfBatsman(matches, deliveries);
saveDataToJSON(result, "batsmanStrikeRateBySeason.json");
