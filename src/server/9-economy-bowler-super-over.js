const data = require('../data/data');
const saveDataToJSON = require("../server/index");

const deliveries = data().deliveries();

function bowlerInSuperOver(deliveries) {
  const bowlerSuperOver = {};

  for (let delivery of deliveries) {
    if (delivery.is_super_over !== '0') {
      const isLegalBall = delivery.wide_runs === '0' && delivery.noball_runs === '0';

      if (!bowlerSuperOver[delivery.bowler]) {
        bowlerSuperOver[delivery.bowler] = { runs: 0, balls: 0 };
      }

      if (isLegalBall) {
        bowlerSuperOver[delivery.bowler].balls++;
      }

      bowlerSuperOver[delivery.bowler].runs += parseInt(delivery.total_runs, 10);
    }
  }

  const economyRates = [];
  for (let bowler in bowlerSuperOver) {
    let overs = bowlerSuperOver[bowler].balls / 6;
    let economy = bowlerSuperOver[bowler].runs / overs;
    economyRates.push({ bowler, economy });
  }

  economyRates.sort((a, b) => a.economy - b.economy);

  
  const bestBowler = economyRates.length > 0 ? economyRates[0] : null;
  return bestBowler;
}

const result = bowlerInSuperOver(deliveries);
saveDataToJSON(result, "bestEconomicalRateInSuperOverBowler.json");
